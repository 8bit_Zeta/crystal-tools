# Using readlines()
file1 = open('./tempMoves.txt', 'r')
Lines = file1.readlines()

newAtkString = ''

for line in Lines:
    wordList = line.strip().split('_')

    atkName = ''

    for word in wordList:
        atkName = atkName + word.title()

    newAtkString = newAtkString + atkName + 'Description:\n' +\
    '\tdb   "Pounds with fore-"\n' +\
    '\tnext "legs or tail.@"\n\n'

print(newAtkString)
# writing to file
file1 = open('newAtkDescription.txt', 'w')
file1.write(newAtkString)
file1.close()