# Using readlines()
file1 = open('./tempMoves.txt', 'r')
Lines = file1.readlines()

newAtkString = ''

for line in Lines:
    wordList = line.strip().split('_')

    atkName = ''

    for word in wordList:
        atkName = atkName + word.title()

    newAtkString = newAtkString + '\tdw ' + atkName + 'Description\n'

print(newAtkString)
# writing to file
file1 = open('newAtkDescriptionTitle.txt', 'w')
file1.write(newAtkString)
file1.close()
