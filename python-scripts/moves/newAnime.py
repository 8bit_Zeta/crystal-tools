# Using readlines()
file1 = open('./tempMoves.txt', 'r')
Lines = file1.readlines()

newAtkString = ''

for line in Lines:
    wordList = line.strip().split('_')

    atkName = ''

    for word in wordList:
        atkName = atkName + word.title()

    newAtkString = newAtkString + 'BattleAnim_' + atkName + ':\n' +\
    '\tanim_1gfx ANIM_GFX_HIT\n' +\
    '\tanim_sound 0, 1, SFX_POUND\n' +\
    '\tanim_obj ANIM_OBJ_08, 136, 56, $0\n' +\
    '\tanim_wait 6\n' +\
    '\tanim_obj ANIM_OBJ_01, 136, 56, $0\n' +\
    '\tanim_wait 16\n' +\
    '\tanim_ret\n\n'

print(newAtkString)
# writing to file
file1 = open('newAtkAnim.txt', 'w')
file1.write(newAtkString)
file1.close()