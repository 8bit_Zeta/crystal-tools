#Packages
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import urllib
import re
import pandas as pd
import numpy as np
import requests
from urllib.request import urlopen
import csv
import time
import re
import textwrap

#Set-up
url = "https://pokemondb.net/pokedex/"
pageurl = url + "all"
response = requests.get(pageurl)

print("Pulling list of Pokémon from https://pokemondb.net/pokedex/all")

pokeList = []

page = response.text
soup = BeautifulSoup(page, 'html.parser')

all_matches = soup.find_all('a', {"class": "ent-name"})

all_matches = list(dict.fromkeys(all_matches))

for i in all_matches:
    pokeList.append(url + i.text)

opts = Options()
opts.headless = True

driver = webdriver.Firefox(options=opts)

pokemonStart = int(input("What Pokémon number would you like to begin with? ")) - 1
pokemonEnd = int(input("What Pokémon number would you like to end with? "))

formBool = True
formInput = input("Would you like to pull data for Pokémon forms? (Y/n) ")

if formInput == 'n' or formInput == 'N':
    formBool = False

totalPokeList = []

#Scraping Pokemondb
for poke in pokeList[pokemonStart:pokemonEnd]: #Pokemon total 953

    print("Processing Data for " + poke.split("/")[-1].title())

    driver.get(poke)

    content = driver.page_source.encode('utf-8').strip()
    p_soup = BeautifulSoup(content,"html.parser")

    # # =====================================================================

    dexBasicDiv = p_soup.find("div", {"id": "dex-basics"})

    currentElement = dexBasicDiv
    foundFormsBool = False
    formList = []

    while foundFormsBool != True:
        currentElement = currentElement.next_element

        if currentElement.name == 'div':
            if currentElement.attrs['class'][0] == 'sv-tabs-tab-list':
                foundFormsBool = True
                
                formListTags = currentElement.findAll('a')

                for entry in formListTags:
                    formList.append(entry.text)

    currentFormList = []

    thList = p_soup.findAll("th")

    formIndex = 0

    for i in thList:
        
        # if i.text.split(" ")[0] == 'National':

        #     currentElement = i
        #     while currentElement.name != 'td':
        #         currentElement = currentElement.next_element

        #     print(currentElement.text)

        if i.text == 'Species':

            currentElement = i
            while currentElement.name != 'td':
                currentElement = currentElement.next_element

            currentFormList.append(currentElement.text)

        elif i.text == 'Height':

            currentElement = i
            while currentElement.name != 'td':
                currentElement = currentElement.next_element

            currentFormList.append(currentElement.text)

        elif i.text == 'Weight':

            currentElement = i
            while currentElement.name != 'td':
                currentElement = currentElement.next_element

            currentFormList.append(currentElement.text)
            currentFormList.insert(0, formList[formIndex])

            totalPokeList.append(currentFormList)
            currentFormList = []
            formIndex = formIndex + 1

            if formBool == False:
                break

for i in totalPokeList:
    print(i)

    # speciesTable = tables[0].prettify().split('\n')

    # speciesTable = [x.strip(' ') for x in speciesTable]

    # #Find Species Name
    # speciesName = speciesTable[30].split(" ")[0]

    # #Find Height
    # height = re.sub('|\(|\)|\′|\″', '', speciesTable[38].split(" ")[1])

    # #Find Weight
    # weight = re.sub('|\(|\)|\.', '', speciesTable[46].split(" ")[1])
    # weight = weight[:len(weight) - 4]    

    # # =====================================================================

#     h2List = p_soup.findAll("h2")
    
#     for i in h2List:
#         if i.text == 'Pokédex entries':
            
#             currentElement = i
#             while currentElement.name != 'table':
#                 currentElement = currentElement.next_element

#     dexTable = currentElement.prettify().split('\n')

#     dexTable = [x.strip(' ') for x in dexTable]
#     # dexTable = [x for x in dexTable if len(x) >= 20]

#     tempList = []
#     dexEntryList = []
    
#     for index, line in enumerate(dexTable):

#         lineSplit = line.split(" ")

#         if lineSplit[0] == "<span":
#             tempList.append(lineSplit[2][:len(lineSplit[2]) - 2])

#         elif lineSplit[0] == "<td":
#             for game in tempList:
#                 dexEntryList.append((game, dexTable[index + 1]))

#             tempList.clear()

#     # =====================================================================

#     formatDexEntryString = ""

#     for entry in dexEntryList:
#         if entry[0] == 'platinum':
#             formatDexEntryString = entry[1]

#     # Wrap this text. 
#     wrapper = textwrap.TextWrapper(width=18) 

#     word_list = wrapper.wrap(text=formatDexEntryString)

#     formatDexEntry = textwrap.fill(formatDexEntryString, width=18)

#     formatDexEntryLines = formatDexEntry.split('\n')

#     speciesLine = 'db "' + speciesName.upper() + '@" ; species name\n'
#     heightWeightLine = 'dw ' + height + ', ' + weight + ' ; height, weight\n\n'

#     dexLines = []

#     for index, line in enumerate(formatDexEntryLines):
        
#         if index == 0:
#             dexLines.append('db   "' + line + '"\n')

#         elif index == 3:
#             dexLines.append('\npage "' + line + '"\n')

#         elif index == len(formatDexEntryLines) - 1:
#             dexLines.append('next "' + line + '@"\n')

#         else:
#             dexLines.append('next "' + line + '"\n')



#     outputString = speciesLine + heightWeightLine

#     for x in dexLines:
#         outputString = outputString + x



#     print("Generating File for " + poke.split("/")[-1].title())

#     # writing to file
#     file1 = open('../Data/' + poke.split("/")[-1].lower() + '.asm', 'w')
#     file1.write(outputString)
#     file1.close()

driver.close()

# # =====================================================================

