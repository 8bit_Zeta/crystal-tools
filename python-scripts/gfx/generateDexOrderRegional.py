# Using readlines()
file1 = open('./pokemonName.txt', 'r')
Lines = file1.readlines()

# Using readlines()
file2 = open('./regionalOrder.txt', 'r')
regionalOrder = file2.readlines()

newDexString = ''

for entry in regionalOrder:
    entryNum = int(entry) - 1
    newDexString = newDexString + '\tdw ' + Lines[entryNum].strip() + '\n'

print(newDexString)
# writing to file
file1 = open('newDexOrder_Regional.txt', 'w')
file1.write(newDexString)
file1.close()
