import shutil
from os import walk

loopVar = 'y'

while loopVar == 'y' or loopVar == 'Y':

    region = input("What region would you like to generate?  ")

    _, _, filenames = next(walk('/home/devin/Documents/ancientplatinum/data/pokemon/base_stats/' + region))

    for file in filenames:
        size = len(file)
        currentMon = file[:size - 4]
        shutil.copy("/home/devin/Documents/ancientplatinum/data/pokemon/dex_entries/pikachu.asm", "/home/devin/Documents/ancientplatinum/data/pokemon/dex_entries/" + region + "/" + currentMon + ".asm")

    loopVar = input("Would you like to add another region? (Y/n)\t") or "y"