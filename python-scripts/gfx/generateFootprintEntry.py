from os import walk

loopVar = 'y'

while loopVar == 'y' or loopVar == 'Y':

    region = input("What region would you like to generate?  ")

    _, _, filenames = next(walk('/home/devin/Documents/ancientplatinum/data/pokemon/base_stats/' + region))

    tupleList = []
    outputString = ''

    for file in filenames:
        size = len(file)
        currentMon = file[:size - 4]
        
        # Using readlines()
        file1 = open('/home/devin/Documents/ancientplatinum/data/pokemon/base_stats/' + region + '/' + file, 'r')
        Line = file1.readline()

        dexNumber = Line[8] + Line[9] + Line[10]

        footprintTuple = ('INCBIN "gfx/footprints/' + region + '/' + currentMon + '.1bpp"\n', dexNumber)

        tupleList.append(footprintTuple)

    for item in sorted(tupleList, key=lambda x: x[1]):
        outputString = outputString + item[0]

    # writing to file
    file1 = open('newFootprintEntry_' + region.title() + '.txt', 'w')
    file1.write(outputString)
    file1.close()

    loopVar = input("Would you like to add another region? (Y/n)\t") or "y"

