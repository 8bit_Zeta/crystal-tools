from distutils.dir_util import copy_tree
from os import walk

loopVar = 'y'

while loopVar == 'y' or loopVar == 'Y':

    region = input("What region would you like to generate?  ")

    _, _, filenames = next(walk('/home/devin/Documents/ancient-engine/data/pokemon/base_stats/' + region))

    for file in filenames:
        size = len(file)
        currentMon = file[:size - 4]
        copy_tree("/home/devin/Documents/ancient-engine/gfx/pokemon/pikachu", "/home/devin/Documents/ancient-engine/gfx/pokemon/" + region + "/" + currentMon)

    loopVar = input("Would you like to add another region? Y/n") or "y"